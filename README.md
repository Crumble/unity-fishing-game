![Preview](https://i.imgur.com/jdGB5cO.mp4)

UNITY 2018.1

A cool fishing mini game i made for the GDL Fishing Jam > https://itch.io/jam/gdl-fishing-jam/rate/204657

Made using a State machine for the fish controllers.

Left Click a fish to tell them to follow your mouse, Lead them into a hook with a matching color.
Right click to have them go back to randomly swimming.

Each fish you catch increases the time remaining. Try to play as long as you can!

You can test it out [here](https://crumble.gitlab.io/unity-fishing-game/)

